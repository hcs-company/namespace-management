# Namespace Management

An example of how to manage team namespaces using ArgoCD and `kustomize`
`replacements`.

## Architecture

1. The cluster runs a central ArgoCD in the `openshift-gitops` namespace.
2. Each teams has a name (e.g. `foo`), and that name is used both as their
   groupname and as the prefix for all their namespaces.
3. Teams get their own ArgoCD instance in a namespace called `<team>-cicd`, e.g. `foo-cicd`

## Deploy

1. Create a new AppProject for the namespaces:
   ```bash
   oc apply -f deploy/namespace-appproject.yaml -n openshift-gitops
   ```

2. Create the Application that gives ArgoCd permissions to manage quota related objects:
   ```bash
   oc apply -f deploy/gitops-roles-application.yaml
   ```

3. Create an applicationset that uses the newly created project
   ```bash
   oc apply -f deploy/applicationset.yaml -n openshift-gitops
   ```

